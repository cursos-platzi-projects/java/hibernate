package com.gianni.hibernate.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity/*Se aplica a la clase e indica que esta clase Java es una entidad a persistir.
Tambien es mejor usar la anotacion de @Entity : javax.persistence , por que la de org.hibernate.annotations esta un poco descontinuada*/
@Table(name="social_media")//Se aplica a la clase e indica el nombre de la tabla de la base de datos donde se persistirá la clase.
public class SocialMedia implements Serializable {
	@Id//Este atributo es la clave primaria.
	@Column(name="id_social_media")
	@GeneratedValue(strategy=GenerationType.IDENTITY)//Este ID sera unico
	private Long idSocialMedia; // En las anotaciones anterior significa a que este ID sera unico
	
	@Column(name="name")
	private String name;
	
	@Column(name="icon")
	private String icon;
	
	@OneToMany
	@JoinColumn(name="id_social_media")
	private Set<TeacherSocialMedia> teacherSocialMedia;
	
	public SocialMedia(String name, String icon) {
		super();
		this.name = name;
		this.icon = icon;
	}
	
	public SocialMedia() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getIdSocialMedia() {
		return idSocialMedia;
	}
	public void setIdSocialMedia(Long idSocialMedia) {
		this.idSocialMedia = idSocialMedia;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Set<TeacherSocialMedia> getTeacherSocialMedia() {
		return teacherSocialMedia;
	}

	public void setTeacherSocialMedia(Set<TeacherSocialMedia> teacherSocialMedia) {
		this.teacherSocialMedia = teacherSocialMedia;
	}
	
	
}
