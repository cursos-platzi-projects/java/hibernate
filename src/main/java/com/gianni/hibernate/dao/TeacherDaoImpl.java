package com.gianni.hibernate.dao;

import java.util.List;

import com.gianni.hibernate.model.Teacher;

//Esta clase es usada para poder implementar los metodos y poder usarlos en otra clase que seria la clase main.
//Extendemos la clase PlatziSession para poder abrir la conexion
public class TeacherDaoImpl extends PlatziSession implements TeacherDao {
	
	private PlatziSession platziSession;
	
	/*Esto es para cuando se haga una instancia a la clase TeacherDaoImpl tambien haga una instancia a PlatziSession y
	pueda quedar abierto la sesion*/
	public TeacherDaoImpl() {
		platziSession = new PlatziSession();
	}

	public void saveTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		platziSession.getSession().persist(teacher);//getSession() para devolver la session y ejecutar el metodo persist()
		platziSession.getSession().getTransaction().commit();//Para que se ejecute la transaccion, haga un commit a la query persist()
	}

	public void deleteTeacherById(Long idTeacher) {
		// TODO Auto-generated method stub
		
	}

	public void updateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		
	}

	public List<Teacher> findAllTeachers() {
		// TODO Auto-generated method stub
		//Traeme todos los maestros como un select * from teacher
		//se le pone Teacher en el parametro "" por la clase que estamos llamando
		//Aqui estamos empleando HQL
		return platziSession.getSession().createQuery("from Teacher").list();//Lo que recibe createQuery("from Teacher")
	}

	public Teacher findById(Long idTeacher) {
		// TODO Auto-generated method stub
		return null;
	}

	public Teacher findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
