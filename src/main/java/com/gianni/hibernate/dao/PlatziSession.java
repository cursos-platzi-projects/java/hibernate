package com.gianni.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class PlatziSession {
	
	private Session session;
	//Para que se haga una vez cuando se instace el objeto
	public PlatziSession() {
        Configuration configuration = new Configuration();
        configuration.configure();//Accede al archivo de nuestra config de la base de datos
        SessionFactory sessionFactory = configuration.buildSessionFactory();//Habre la conexion
        session = sessionFactory.openSession();
        session.beginTransaction();//Para que se quede inicializada la transaccion
	}

	//Para que obtenge la session cuando esta ya sea instanciada
	public Session getSession(){
        return session;
	}
	
}
