package com.gianni.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.gianni.hibernate.dao.TeacherDaoImpl;
import com.gianni.hibernate.model.Course;
import com.gianni.hibernate.model.SocialMedia;
import com.gianni.hibernate.model.Teacher;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        Teacher teacher = new Teacher("Christian", "Avatar");//Para crear los datos del teacher
        TeacherDaoImpl teacherDaoImpl = new TeacherDaoImpl();//Para que se inicie la sesion
        teacherDaoImpl.saveTeacher(teacher);//Para poder usar los metodos de la clase TeacherDaoImpl
        
        //Para que nos devuelva una lista de teachers
        List<Teacher> teachers = teacherDaoImpl.findAllTeachers();
        
        //Se hace un foreach de mejor manera para poder obtener la data de los teacher, en ejemplo su nombre.
        for(Teacher t: teachers){
        	System.out.println("Nombre: "+ t.getName());
        }
        
        /*Course course = new Course("Matematicas", "Ecuaciones cuadraticas", "Formulas");
        session.beginTransaction();//Es importante para poder alterar los datos
        session.save(course);
        session.save(teacher);
        session.getTransaction().commit();*/
    }
}
